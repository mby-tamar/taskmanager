import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {
    baseUrl = environment.baseUrl;
    constructor(private http: HttpClient) { }

    getUser(userName: string, password: string): Observable<any> {
        return this.http.post<any>(`${this.baseUrl}/Internal/MFT/getOneUser`, { userName: userName ,password:password});
    }
    
    register(user: User) {
        return this.http.post<any>(`${this.baseUrl}/Internal/BI/register`,{ userName: user.userName ,password:user.password});
    }

}