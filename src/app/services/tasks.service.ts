import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class TaskService {
    baseUrl = environment.baseUrl;
    constructor(private http: HttpClient) { }

    addTask(userId: number, task:Task): Observable<any> {
        return this.http.post<any>(`${this.baseUrl}/Internal/BI/addTask`, 
        {
            userId: userId,
            title: task.title,
            descreption: task.descreption
        })
    }

    getTasks(userId:number) {
        debugger;
        return this.http.post<any>(`${this.baseUrl}/Internal/BI/getUserTasks`,{ userId: userId});
    }

    doTask(taskId: number) {
        return this.http.post<any>(`${this.baseUrl}/Internal/BI/doTask`, { taskId: taskId})
    }
}