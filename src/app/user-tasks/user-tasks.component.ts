import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupName, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../services/tasks.service';
import { Task } from '../models/task.model';
@Component({
  selector: 'app-user-tasks',
  templateUrl: './user-tasks.component.html',
  styleUrls: ['./user-tasks.component.css']
})
export class UserTasksComponent implements OnInit {

  userName: string | null;
  userId: number = 0;
  tasks: Task[] = [];
  
  tasksFrm: FormGroup = new FormGroup({
    title: new FormControl<string>('', Validators.required),
    descreption: new FormControl<string>(''),
    done: new FormControl<boolean>(false)
  });

  
  constructor(private taskService: TaskService, private router: Router, private route: ActivatedRoute) {
    this.userId = Number(route.snapshot.paramMap.get('userId'));
    this.userName = route.snapshot.paramMap.get('userName');
  }

  ngOnInit(): void {
    this.getTasks();
  }
  getTasks() {
    this.taskService.getTasks(this.userId).subscribe((t: any) => {
      debugger;
      this.tasks = t['tasks'];  
    });

  }

  addTask() {
    let task = {
      id: 0,
      userId: Number(this.userId),
      title: this.tasksFrm.controls['title'].value,
      descreption: this.tasksFrm.controls['descreption'].value,
      done: false,
      date: new Date()
    }

    this.taskService.addTask(this.userId, task).subscribe((task: any) => {
      alert(`המשימה נוספה בהצלחה`);
      this.getTasks();
    });
  }
  
  doTask(task: Task) {
    this.taskService.doTask(task.id).subscribe(() => {
      this.getTasks();
    });
  }
}
