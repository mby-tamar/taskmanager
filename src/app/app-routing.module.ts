 import { NgModule } from '@angular/core';
 import { RouterModule, Routes } from '@angular/router';
 import { HomeComponent } from './home/home.component';
 import { LoginComponent } from './login/login.component';
 import { RegisterComponent } from './register/register.component';
import { UserTasksComponent } from './user-tasks/user-tasks.component';


const routes: Routes = [
   // { path: '', component: HomeComponent },
    { path: '', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'userTasks', component: UserTasksComponent}

];

export const appRoutingModule = RouterModule.forRoot(routes);
